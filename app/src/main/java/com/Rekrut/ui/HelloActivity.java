package com.Rekrut.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.Rekrut.R;
import com.Rekrut.ui.ble.BluetoothActivity;

public class HelloActivity extends Activity {

    private static final int DELAY = 1900;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.hello_ativity);
            findViewById(R.id.angry_btn).setOnClickListener(v -> bluetoothActivityStart());
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            findViewById(R.id.angry_btn).startAnimation(shake);
        }

        public void bluetoothActivityStart(){

            Intent intent = new Intent(this, BluetoothActivity.class);
            startActivity(intent);
        }



    }