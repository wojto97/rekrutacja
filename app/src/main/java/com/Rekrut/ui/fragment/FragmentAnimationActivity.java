package com.Rekrut.ui.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.Rekrut.R;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class FragmentAnimationActivity extends AppCompatActivity {
    private static final int MENU_COLOR = android.R.color.holo_green_light;
    private static final int PROFILE_COLOR = android.R.color.holo_purple;
    private static final int SETTINGS_COLOR = android.R.color.holo_red_light;
    private final MenuPagerAdapter mPagerAdapter = new MenuPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activivty_fragment_animation);
        createBottomNavigation();
    }

    private void createBottomNavigation() {
        AHBottomNavigation bottomNavigation = findViewById(R.id.bnMain);
        AHBottomNavigationViewPager viewPager = findViewById(R.id.view_pager);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.navigation_tab_menu,
                R.drawable.outline_menu_white_24, MENU_COLOR);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.navigation_tab_profile,
                R.drawable.outline_face_white_24, PROFILE_COLOR);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.navigation_tab_settings,
                R.drawable.outline_settings_white_24, SETTINGS_COLOR);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);

        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.setColored(true);
        bottomNavigation.setAccentColor(ContextCompat.getColor(this, android.R.color.white));
        bottomNavigation.setInactiveColor(ContextCompat.getColor(this, android.R.color.darker_gray));
        bottomNavigation.setNotificationTextColor(ContextCompat.getColor(this, android.R.color.white));
        bottomNavigation.setNotificationBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark));

        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            if (!wasSelected) {
                changeColors(position);
                viewPager.setCurrentItem(position, false);
            }
            return true;
        });

        viewPager.setOffscreenPageLimit(mPagerAdapter.getCount());
        viewPager.setAdapter(mPagerAdapter);
        changeColors(0);
    }

    private void changeColors(int position) {
        int color = 0;
        switch (position) {
            case 0:
                color = MENU_COLOR;
                break;
            case 1:
                color = PROFILE_COLOR;
                break;
            case 2:
                color = SETTINGS_COLOR;
                break;
        }
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                .getColor(color)));
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, color));
    }

    public class MenuPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private Fragment mCurrentFragment;

        /* package */ MenuPagerAdapter(@NonNull FragmentManager fragmentManager) {
            super(fragmentManager);

            mFragments.clear();
            mFragments.add(new MenuFragment());
            mFragments.add(new ProfileFragment());
            mFragments.add(new SettingsFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentFragment() != object) {
                mCurrentFragment = (Fragment) object;
            }

            super.setPrimaryItem(container, position, object);
        }

        private @NonNull Fragment getCurrentFragment() {
            return mCurrentFragment;
        }
    }

}
